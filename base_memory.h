#ifndef BASE_MEMORY_H
#define BASE_MEMORY_H

#define ARENA_MIN_SIZE 64
#define ARENA_ALIGNMENT_UNSPECIFIED 8
#define ARENA_DEF_RESERVE_SIZE Megabytes(64)
#define ARENA_COMMIT_GRANULARITY Kilobytes(64)

typedef u32 ArenaFlags;
enum
{
    ArenaFlag_Chained = (1 << 0),
    ArenaFlag_MMU = (1 << 1),
};

typedef struct Arena Arena;
struct Arena
{
    ArenaFlags flags;
    Arena *current;
    Arena *prev;
    u64 pos;
    u64 commit;
    u64 base;
    u64 cap;
};

typedef struct TempArena TempArena;
struct TempArena
{
    Arena *arena;
    u64 pos;
};

function Arena* ArenaAllocSize(ArenaFlags flags, u64 reserve, u64 commit);
function Arena* ArenaAlloc(ArenaFlags flags);
function void   ArenaRelease(Arena *arena);
function u64    ArenaGetPos(Arena *arena);
function void*  ArenaPush(Arena *arena, u64 size, u64 align, b32 zero);
#define ArenaPushN(arena, type, count, zero) (type *)ArenaPush(arena, sizeof(type)*(count), AlignOf(type), (zero));
function void   ArenaPopTo(Arena *arena, u64 pos);
function void   ArenaPop(Arena *arena, u64 amount);
function void   ArenaClear(Arena *arena);

function TempArena ArenaTempBegin(Arena *arena);
function void      ArenaTempEnd(TempArena temp);

#endif //BASE_MEMORY_H