#ifndef MemImpl_Reserve
# error Missing implementation for MemImpl_Reserve(u64) -> void*
#endif
#ifndef MemImpl_Commit
# error Missing implementation for MemImpl_Commit(void*, u64) -> void
#endif
#ifndef MemImpl_Decommit
# error Missing implementation for MemImpl_Decommit(void*, u64) -> void
#endif
#ifndef MemImpl_Release
# error Missing implementation for MemImpl_Release(void*, u64) -> void
#endif

StaticAssert(sizeof(Arena) <= ARENA_MIN_SIZE, arena_check_size);
function Arena* 
ArenaAllocSize(ArenaFlags flags, u64 reserve, u64 commit)
{
    Assert(commit <= reserve && commit >= ARENA_MIN_SIZE);
    Arena *result = 0;
    result = (Arena *)MemImpl_Reserve(reserve);
    MemImpl_Commit(result, commit);
    if (result) {
        MemoryZero(result, sizeof(Arena));
        result->flags = flags;
        result->current = result;
        result->pos = ARENA_MIN_SIZE;
        result->commit = commit;
        result->cap = reserve;
    }
    
    return result;
}

function Arena* 
ArenaAlloc(ArenaFlags flags)
{
    Arena *result = 0;
    if (flags & ArenaFlag_MMU)
        result = ArenaAllocSize(flags, ARENA_DEF_RESERVE_SIZE, ARENA_COMMIT_GRANULARITY);
    else
        result = ArenaAllocSize(flags, ARENA_COMMIT_GRANULARITY, ARENA_COMMIT_GRANULARITY);
    
    return result;
}

function void   
ArenaRelease(Arena *arena)
{
    if (arena->flags & ArenaFlag_Chained) {
        for (Arena *node = arena->current, *prev = 0; node != 0; node = prev) {
            prev = node->prev;
            MemImpl_Release(node, node->cap);
        } 
    } else {
        MemImpl_Release(arena, arena->cap);
    }
}

function u64    
ArenaGetPos(Arena *arena)
{
    u64 pos = arena->current->pos;
    if (arena->flags & ArenaFlag_Chained) 
        pos += arena->current->base;
    
    return pos;
}

function void*  
ArenaPush(Arena *arena, u64 size, u64 align, b32 zero)
{
    Assert(IsPow2(align));
    void *result = 0;
    Arena *current = arena->current;
    u64 alignedPos = AlignUpPow2(current->pos, align);
    u64 diff = alignedPos - current->pos;
    u64 newPos = alignedPos + size;
    result = (u8 *)current + alignedPos;
    current->pos = newPos;
    
    if (current->pos > current->commit) {
        result = 0;
        current->pos -= diff + size;
        
        if (current->flags & ArenaFlag_Chained && newPos > current->cap) {
            Arena *newBlock = 0;
            if ((size >= ARENA_DEF_RESERVE_SIZE && current->flags & ArenaFlag_MMU) || 
                (size >= ARENA_COMMIT_GRANULARITY && current->flags ^ ArenaFlag_MMU)) {
                u64 newSize = size + ARENA_MIN_SIZE;
                u64 alignedNewSize = AlignUpPow2(newSize, Kilobytes(4)); // Change?
                newBlock = ArenaAllocSize(current->flags, alignedNewSize, alignedNewSize);
            } else {
                newBlock = ArenaAlloc(current->flags);
            }
            if (newBlock) {
                newBlock->base = current->base + current->pos;
                newBlock->prev = current;
                arena->current = newBlock;
                current = newBlock;
                
                alignedPos = current->pos;
                newPos = alignedPos + size;
            }
        }
        
        if (newPos <= current->cap) {
            if (current->flags & ArenaFlag_MMU && newPos > current->commit) {
                u64 commitSize = newPos - current->commit;
                u64 roundedCommitSize = AlignUpPow2(commitSize, ARENA_COMMIT_GRANULARITY);
                u64 newCommitSize = ClampTop(roundedCommitSize, current->cap);
                MemImpl_Commit((u8 *)current + current->commit, newCommitSize);
                current->commit += newCommitSize;
            }
            
            if (newPos <= current->commit) {
                result = (u8 *)current + alignedPos;
                current->pos = newPos;
            }
        }
    }
    
    if (result && zero) {
        MemoryZero(result, size);
    }
    
    return result;
}

function void   
ArenaPopTo(Arena *arena, u64 pos)
{
    if (pos < ArenaGetPos(arena)) {
        u64 newPos = Max(pos, ARENA_MIN_SIZE);
        if (arena->flags & ArenaFlag_Chained) {
            Arena *node = arena->current;
            for (Arena *prev = 0; node != 0 && node->base >= newPos; node = prev) {
                prev = node->prev;
                MemImpl_Release(node, node->cap);
            }
            arena->current = node;
            
            u64 localPos = pos - arena->current->base;
            u64 localPosClamped = Max(localPos, ARENA_MIN_SIZE);
            arena->current->pos = localPosClamped;
        } else {
            arena->pos = newPos;
        }
        
        if (arena->flags & ArenaFlag_MMU) {
            Arena *current = arena->current;
            u64 nearCommit = AlignUpPow2(current->pos, ARENA_COMMIT_GRANULARITY);
            if (nearCommit < current->commit) {
                u64 decommitSize = current->commit - nearCommit;
                MemImpl_Decommit((u8 *)current + nearCommit, decommitSize);
                current->commit -= decommitSize;
            }
        }
    }
}

function void   
ArenaPop(Arena *arena, u64 amount)
{
    ArenaPopTo(arena, ArenaGetPos(arena) - amount);
}

function void   
ArenaClear(Arena *arena)
{
    ArenaPopTo(arena, 0);
}

function TempArena 
ArenaTempBegin(Arena *arena)
{
    TempArena result = {arena, ArenaGetPos(arena)};
    return result;
}

function void
ArenaTempEnd(TempArena temp)
{
    ArenaPopTo(temp.arena, temp.pos);
}