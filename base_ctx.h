#ifndef BASE_CTX_H
#define BASE_CTX_H

//~ Compiler, platform, and architecture extraction

#if defined(_MSC_VER)

# define COMPILER_MSVC 1
# if defined(_WIN32)
#  define OS_WINDOWS 1
# else
#  error MSVC is only supported on Windows
# endif

# if defined(_M_AMD64)
#  define ARCH_X64 1
# elif defined(_M_IX86)
#  define ARCH_X86 1
# elif defined(_M_ARM64)
#  define ARCH_ARM64 1
# elif defined(_M_ARM)
#  define ARCH_ARM32 1
# else
#  error Target architecture is not supported on MSVC
# endif

#elif defined(__clang__)

# define COMPILER_CLANG 1
# if defined(__APPLE__) && defined(__MACH__)
#  define OS_MAC 1
# elif defined(__gnu_linux__)
#  define OS_LINUX 1
# else
#  error Target platform is not supported on Clang
# endif

# if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)
#  define ARCH_X64 1
# elif defined(i386) || defined(__i386) || defined(__i386__)
#  define ARCH_X86 1
# elif defined(__aarch64__)
#  define ARCH_ARM64 1
# elif defined(__arm__)
#  define ARCH_ARM32 1
# else
#  error Target architecture is not supported on Clang
# endif

#elif defined(__GNUC__) || defined(__GNUG__)

# define COMPILER_GCC 1
# if defined(__gnu_linux__)
#  define OS_LINUX 1
# else
#  error Target platform is not supported on GCC
# endif

# if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)
#  define ARCH_X64 1
# elif defined(i386) || defined(__i386) || defined(__i386__)
#  define ARCH_X86 1
# elif defined(__aarch64__)
#  define ARCH_ARM64 1
# elif defined(__arm__)
#  define ARCH_ARM32 1
# else
#  error Target architecture is not supported on GCC
# endif

#else
# error Compiler is not supported. _MSC_VER, __clang__, __GNUC__, or __GNUG__ must be defined.
#endif

#if ARCH_X64 || ARCH_ARM64
# define ARCH_64BIT 1
#endif

//~ Build options

#if !defined(BUILD_DEBUG)
# pragma message("Define BUILD_DEBUG=1 to enable debug features and BUILD_DEBUG=0 for a cleaner release executable")
#endif

//~ Language

#if defined(__OBJC__)
# define LANG_OBJC 1
#endif

#if defined(__cplusplus)
# define LANG_CPP 1
#endif

//~ Zero remaining macros

#if !defined(ARCH_64BIT)
# define ARCH_64BIT 0
#endif
#if !defined(ARCH_X64)
# define ARCH_X64 0
#endif
#if !defined(ARCH_X86)
# define ARCH_X86 0
#endif
#if !defined(ARCH_ARM64)
# define ARCH_ARM64 0
#endif
#if !defined(ARCH_ARM32)
# define ARCH_ARM32 0
#endif
#if !defined(COMPILER_MSVC)
# define COMPILER_MSVC 0
#endif
#if !defined(COMPILER_GCC)
# define COMPILER_GCC 0
#endif
#if !defined(COMPILER_CLANG)
# define COMPILER_CLANG 0
#endif
#if !defined(OS_WINDOWS)
# define OS_WINDOWS 0
#endif
#if !defined(OS_LINUX)
# define OS_LINUX 0
#endif
#if !defined(OS_MAC)
# define OS_MAC 0
#endif
#if !defined(LANG_CPP)
# define LANG_CPP 0
#endif
#if !defined(LANG_OBJC)
# define LANG_OBJC 0
#endif

#endif //BASE_CTX_H
