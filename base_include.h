#ifndef BASE_H
#define BASE_H

#include "base_ctx.h"
#include "base_types.h"
#include "base_memory.h"
#include "third_party/stb_sprintf.h"
#include "base_strings.h"
#define HANDMADE_MATH_NO_SSE // Is SSE really the best here? (https://www.reedbeta.com/blog/on-vector-math-libraries/)
#include "third_party/HandmadeMath.h"

#endif //BASE_H
