#ifndef BASE_TYPES_H
#define BASE_TYPES_H

// I eventually want to move away from the CRT, and provide a header which allows for easy ditching
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#if COMPILER_MSVC
# include <intrin.h>
#endif

//~ Macros

//- Keywords

#if LANG_CPP
# define no_mangle extern "C"
#else
# define no_mangle
#endif

#define fallthrough

#define global static
#define function static
#define local_persist static
#define inline_function static inline

#if COMPILER_MSVC
# define thread_storage __declspec(thread)
#elif COMPILER_CLANG || COMPILER_GCC
# define thread_storage __thread
#else
# error Thread local storage is not supported by this compiler
#endif

#if COMPILER_MSVC
# pragma section(".roglob", read)
# define read_only __declspec(allocate(".roglob"))
#else
# define read_only
#endif

//- General functions

#define Stmnt(s) do { s } while (0)

#define Swap(type, a, b) Stmnt( type __temp__ =  a; a = b; b = __temp__; )

//- Assert
#if COMPILER_MSVC
# define AssertBreak() __debugbreak()
#else
# define AssertBreak() (*(volatile int *)0 = 0)
#endif

#if BUILD_DEBUG
# define Assert(c) Stmnt( if (!(c)) { AssertBreak(); } )
# define StaticAssert(c, label) u8 static_assert_##label[(c)?(1):(-1)]
# define InvalidPath() Assert(!"Invalid path")
#else 
# define Assert(c)
# define StaticAssert(c, label)
# define InvalidPath()
#endif

//- Memory operations

#define MemoryCopy memcpy
#define MemoryMove memmove
#define MemorySet memset
#define MemoryCompare memcmp

// StaticAssert()?
#define MemoryCopyStruct(dest, src) Stmnt( Assert(sizeof(*(dest)) == sizeof(*(src))); MemoryCopy((dest), (src), sizeof(*(dest))); )
#define MemoryCopyArray(dest, src) Stmnt( Assert(sizeof((dest)) == sizeof((src))); MemoryCopy((dest), (src), sizeof((dest))); )

#define MemoryMatch(a, b, z) (MemoryCompare((a), (b), (z)) == 0)

#define MemoryZero(ptr, size) MemorySet((ptr), 0, (size))
#define MemoryZeroStruct(ptr) MemorySet((ptr), 0, sizeof(*(ptr)))
#define MemoryZeroArray(arr) MemorySet((arr), 0, sizeof((arr)))

//- Compiler silencers

#define UnusedVariable(var) (void *)var

//- Linked lists

#define DLLPushBack_NP(f, l, n, next, prev) \
((f)==0?\
((f)=(l)=(n),(n)->next=(n)->prev=0):\
((n)->prev=(l),(l)->next=(n),(l)=(n),(n)->next=0))

#define DLLRemove_NP(f, l, n, next, prev) \
((f)==(n)?\
((f)==(l)?\
((f)=(l)=(0)):\
((f)=(f)->next,(f)->prev=0)):\
(l)==(n)?\
((l)=(l)->prev,(l)->next=0):\
((n)->next->prev=(n)->prev,\
(n)->prev->next=(n)->next))

#define DLLPushBack(f, l, n) DLLPushBack_NP(f, l, n, next, prev)
#define DLLPushFront(f, l, n) DLLPushBack_NP(f, l, n, prev, next)
#define DLLRemove(f, l, n) DLLRemove_NP(f, l, n, next, prev)

#define SLLQueuePush_N(f, l, n, next) \
((f)==0?\
(f)=(l)=(n):\
((l)->next=(n),(l)=(n)),\
(n)->next=0)

#define SLLQueuePushFront_N(f, l, n, next) \
((f)==0?\
((f)=(l)=(n),(n)->next=0):\
((n)->next=(f),(f)=(n)))

#define SLLQueuePop_N(f, l, next) \
((f)==(l)?\
(f)=(l)=0:\
(f)=(f)->next)

#define SLLQueuePush(f, l, n) SLLQueuePush_N(f, l, n, next)
#define SLLQueuePushFront(f, l, n) SLLQueuePushFront_N(f, l, n, next)
#define SLLQueuePop(f, l) SLLQueuePop_N(f, l, next)

#define SLLStackPush_N(f, n, next) \
((n)->next=(f),(f)=(n))

#define SLLStackPop_N(f, next) \
((f)==0?0:\
((f)=(f)->next))

#define SLLStackPush(f, n) SLLStackPush_N(f, n, next)
#define SLLStackPop(f) SLLStackPop_N(f, next)

//- Arithmetic manipulations

#define ArrayCount(a) (sizeof((a)) / sizeof((a)[0]))
#define IntFromPtr(p) (u64)(((u8 *)p) - 0)
#define PtrFromInt(i) (void *)(((u8 *)0) + i)
#define Member(type, member_name) ((type *)0)->member_name
#define OffsetOf(type, member_name) IntFromPtr(&Member(type, member_name))
#define BaseFromMember(type, member_name, ptr) (type *)((u8 *)(ptr) - OffsetOf(type, member_name))

#define IsPow2(x) (((x)&((x) - 1)) == 0)
#define AlignUpPow2(x, p) (((x) + (p) - 1)&~((p) - 1))
#define AlignDownPow2(x, p) ((x)&~((p) - 1))

#if COMPILER_MSVC
# define AlignOf __alignof
#elif COMPILER_CLANG || COMPILER_GCC
# define AlignOf __alignof__
#endif

#define Thousand(n) ((n)*1000)
#define Million(n)  ((n)*1000000)
#define Billion(n)  ((n)*1000000000LL)

#define Bytes(n)      (n)
#define Kilobytes(n)  (n << 10)
#define Megabytes(n)  (n << 20)
#define Gigabytes(n)  (((u64)n) << 30)
#define Terabytes(n)  (((u64)n) << 40)

#define Min(a, b) (((a)<(b)) ? (a) : (b))
#define Max(a, b) (((a)>(b)) ? (a) : (b))
#define ClampTop(x, a) Min(x,a)
#define ClampBot(a, x) Max(a,x)
#define Clamp(a, x, b) (((a)>(x))?(a):((b)<(x))?(b):(x))

//- Arithmetic constants

#define u8_max (u8)0xff
#define u8_min (u8)0
#define u16_max (u16)0xffff
#define u16_min (u16)0
#define u32_max (u32)0xffffffff
#define u32_min (u32)0
#define u64_max (u64)0xffffffffffffffff
#define u64_min (u64)0

#define s8_max (s8)0x7f
#define s8_min (s8)(-1 - 0x7f)
#define s16_max (s16)0x7fff
#define s16_min (s16)(-1 - 0x7fff)
#define s32_max (s32)0x7fffffff
#define s32_min (s32)(-1 - 0x7fffffff)
#define s64_max (s64)0x7fffffffffffffff
#define s64_min (s64)(-1 - 0x7FFFFFFFFFFFFFFF)

// TODO: Add other constants for floats such as Pi, etc

//~ Base types

#if !LANG_CPP
# define true 1
# define false 0
#endif
typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef s8 b8;
typedef s16 b16;
typedef s32 b32;
typedef s64 b64;
typedef float f32;
typedef double f64;
typedef void VoidFunc(void);

//- Endianess helpers

inline_function u16
LeToBe16(u16 value)
{
    return ((value << 8) | (value >> 8));
}

inline_function u16
BeToLe16(u16 value)
{
    return ((value >> 8) | (value << 8));
}

//- Symbolic constants

typedef enum Axis
{
    Axis_X,
    Axis_Y,
    Axis_Z,
    Axis_W,
} Axis;

typedef enum Dir
{
    Dir_Up,
    Dir_Down,
    Dir_Left,
    Dir_Right,
} Dir;

typedef enum Side
{
    Side_Min,
    Side_Max,
} Side;

typedef struct DateTime DateTime;
struct DateTime
{
    u16 year;
    u8 month;
    u8 day;
    u8 weekday;
    u8 hour;
    u8 minute;
    u8 second;
    u16 milisecond;
};

#endif //BASE_TYPES_H
