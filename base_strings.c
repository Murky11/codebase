//~ String functions

//- C-String helpers

function u64 
CStringLength(char *str) 
{
    u64 length = 0; 
    for (;str[length]; length++); 
    return length; 
} 

//- Constructors

function String8
Str8(u8 *str, u64 size)
{
    String8 result = {str, size};
    return result;
}

function String8
Str8Range(u8 *first, u8 *opl)
{
    String8 result = {first, (u64)(opl - first)};
    return result;
}

//- Substrings

function String8
Str8Sub(String8 string, u64 first, u64 opl)
{
    u64 firstClamped = ClampTop(first, string.size);
    u64 oplClamped = ClampTop(opl, string.size);
    if (firstClamped > oplClamped) 
        Swap(u64, firstClamped, oplClamped);
    String8 result = {string.str + firstClamped, oplClamped - firstClamped};
    return result;
}

function String8
Str8Chop(String8 string, u64 amount)
{
    u64 amountClamped = ClampTop(amount, string.size);
    String8 result = {string.str, string.size - amountClamped};
    return result;
}

function String8
Str8Skip(String8 string, u64 amount)
{
    u64 amountClamped = ClampTop(amount, string.size);
    String8 result = {string.str + amountClamped, string.size - amountClamped};
    return result;
}

function String8
Str8Prefix(String8 string, u64 size)
{
    u64 sizeClamped = ClampTop(size, string.size);
    String8 result = {string.str, sizeClamped};
    return result;
}

function String8
Str8Postfix(String8 string, u64 size)
{
    u64 sizeClamped = ClampTop(size, string.size);
    u64 skipPos = string.size - sizeClamped;
    String8 result = {string.str + skipPos, sizeClamped};
    return result;
}

//- Allocation

function String8 
Str8PushCpy(Arena *arena, String8 string)
{
    String8 result = {0};
    result.str = ArenaPushN(arena, u8, string.size + 1, true);
    result.size = string.size;
    MemoryCopy(result.str, string.str, string.size);
    return result;
}

function String8 
Str8PushFV(Arena *arena, char *fmt, va_list args)
{
    String8 result = {0};
    va_list argsCpy;
    va_copy(argsCpy, args);
    u64 size = stbsp_vsnprintf(0, 0, fmt, args);
    result.str = ArenaPushN(arena, u8, size + 1, true);
    result.size = size;
    stbsp_vsnprintf((char *)result.str, (int)size, fmt, argsCpy);
    
    return result;
}

function String8 
Str8PushF(Arena *arena, char *fmt, ...)
{
    String8 result = {0};
    va_list args;
    va_start(args, fmt);
    result = Str8PushFV(arena, fmt, args);
    va_end(args);
    
    return result;
}

//- String lists

function void 
Str8ListPushNode(String8List *list, String8Node *node)
{
    SLLQueuePush(list->first, list->last, node);
    list->count++;
    list->size += node->string.size;
}

function void 
Str8ListPushNodeFront(String8List *list, String8Node *node)
{
    SLLQueuePushFront(list->first, list->last, node);
    list->count++;
    list->size += node->string.size;
}

function void 
Str8ListPush(Arena *arena, String8List *list, String8 string)
{
    String8Node *node = ArenaPushN(arena, String8Node, 1, true);
    node->string = string;
    Str8ListPushNode(list, node);
}

function void 
Str8ListPushFront(Arena *arena, String8List *list, String8 string)
{
    String8Node *node = ArenaPushN(arena, String8Node, 1, true);
    node->string = string;
    Str8ListPushNodeFront(list, node);
}

function void 
Str8ListPushF(Arena *arena, String8List *list, char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    String8 string = Str8PushFV(arena, fmt, args);
    Str8ListPush(arena, list, string);
    va_end(args);
}


// TODO: Verify this
function String8List 
Str8Split(Arena *arena, String8 string, u8 *needles, u32 needleCount)
{
    String8List out = {0};
    
    u8 *str = string.str;
    u8 *word = str;
    u8 *opl = string.str + string.size;
    for (u8 *ptr = str; ptr < opl; ++ptr) {
        u8 character = *ptr;
        b32 split = false;
        for (u32 i = 0; i < needleCount; ++i) {
            if (character == needles[i]) {
                split = true;
                break;
            }
        }
        
        if (split) {
            if (word < ptr) 
                Str8ListPush(arena, &out, Str8Range(word, ptr));
            word = ptr + 1;
        }
    }
    
    if (word < str)
        Str8ListPush(arena, &out, Str8Range(word, str));
    
    return out;
}

function String8 
Str8Join(Arena *arena, String8List *list, String8Join *optionalJoinOpts)
{
    String8Join joinOpts = {0};
    if (optionalJoinOpts != 0)
        MemoryCopy(&joinOpts, optionalJoinOpts, sizeof(String8Join));
    
    u64 totalSize = joinOpts.pre.size + (joinOpts.sep.size * ClampBot(list->count - 1, 0)) + joinOpts.post.size + list->size;
    u8 *str = ArenaPushN(arena, u8, totalSize + 1, true);
    
    u8 *ptr = str;
    MemoryCopy(ptr, joinOpts.pre.str, joinOpts.pre.size);
    ptr += joinOpts.pre.size;
    for (String8Node *node = list->first; node != 0; node = node->next) {
        String8 string = node->string;
        MemoryCopy(ptr, string.str, string.size);
        ptr += string.size;
        
        if (node != list->last) {
            MemoryCopy(ptr, joinOpts.sep.str, joinOpts.sep.size);
            ptr += joinOpts.sep.size;
        }
    }
    MemoryCopy(ptr, joinOpts.post.str, joinOpts.post.size);
    ptr += joinOpts.post.size;
    
    String8 result = {str, totalSize};
    return result;
}
