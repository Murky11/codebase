#ifndef BASE_STRINGS_H
#define BASE_STRINGS_H

//~ Basic string types

typedef struct String8 String8;
struct String8
{
    u8 *str;
    u64 size;
};

typedef struct String8Node String8Node;
struct String8Node
{
    String8Node *next;
    String8 string;
};

typedef struct String8List String8List;
struct String8List
{
    String8Node *first;
    String8Node *last;
    u64 count;
    u64 size;
};

typedef struct String8Join String8Join;
struct String8Join
{
    String8 pre, sep, post;
};

//~ String functions

//- C-String helpers

function u64 CStringLength(char *str);

//- Constructors

function String8 Str8(u8 *str, u64 size);
#define Str8C(cstr) Str8((u8 *)(cstr), CStringLength(cstr))
#define Str8Lit(str) Str8((u8 *)(str), sizeof(str) - 1)
#define Str8LitInit(str) {(u8 *)(str), sizeof(str) - 1}
function String8 Str8Range(u8 *first, u8 *opl);

//- Substrings

function String8 Str8Sub(String8 string, u64 first, u64 opl);
function String8 Str8Chop(String8 string, u64 amount);
function String8 Str8Skip(String8 string, u64 amount);
function String8 Str8Prefix(String8 string, u64 size);
function String8 Str8Postfix(String8 string, u64 size);

//- Allocation

function String8 Str8PushCpy(Arena *arena, String8 string);
function String8 Str8PushFV(Arena *arena, char *fmt, va_list args);
function String8 Str8PushF(Arena *arena, char *fmt, ...);

//- String lists

function void Str8ListPushNode(String8List *list, String8Node *node);
function void Str8ListPushNodeFront(String8List *list, String8Node *node);
function void Str8ListPush(Arena *arena, String8List *list, String8 string);
function void Str8ListPushFront(Arena *arena, String8List *list, String8 string);
function void Str8ListPushF(Arena *arena, String8List *list, char *fmt, ...);
function String8List Str8Split(Arena *arena, String8 string, u8 *needle, u32 needleCount);
function String8 Str8Join(Arena *arena, String8List *list, String8Join *optionalJoinOpts); 

//- String formatting

#define Str8Expand(s) (int)((s).size), ((s).str)

//~ Char functions

inline_function b8
CharIsAlphaUpper(u8 c)
{
    return c >= 'A' && c <= 'Z';
}

inline_function b8
CharIsAlphaLower(u8 c)
{
    return c >= 'a' && c <= 'z';
}

inline_function b8 
CharIsAlpha(u8 c)
{
    return CharIsAlphaUpper(c) || CharIsAlphaLower(c);
}

inline_function b8
CharIsDigit(u8 c)
{
    return c >= '0' && c <= '9';
}

inline_function b8
CharIsSymbol(u8 c)
{
    return (c >= '!' && c <= '/') || 
    (c >= ':' && c <= '@') || 
    (c >= '[' && c <= '`') || 
    (c >= '{' && c <= '~');
}

inline_function b8
CharIsWhitespace(u8 c)
{
    return (c == ' ') || (c >= '\a' && c <= '\r');
}

inline_function u8
CharToUpper(u8 c)
{
    return CharIsAlphaLower(c) ? ('A' + (c - 'a')) : c;
}

inline_function u8
CharToLower(u8 c)
{
    return CharIsAlphaUpper(c) ? ('a' + (c - 'A')) : c;
}

inline_function u8
CharToForwardSlash(u8 c)
{
    return c == '\\' ? '/' : c;
}

#endif //BASE_STRINGS_H
